package simulacro.com.eduardorabanal.app.pokedex.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import simulacro.com.eduardorabanal.app.pokedex.R;
import simulacro.com.eduardorabanal.app.pokedex.adapters.PokemonAdapterRV;
import simulacro.com.eduardorabanal.app.pokedex.models.PokemonItem;
import simulacro.com.eduardorabanal.app.pokedex.models.PokemonList;
import simulacro.com.eduardorabanal.app.pokedex.pokeapi.PokeapiService;
import simulacro.com.eduardorabanal.app.pokedex.pokeapi.PokeapiServiceProvider;

public class PokemonListActivity extends AppCompatActivity {
    private static final String TAG = "POKEDEX";
    private PokeapiService service;
    private ArrayList<PokemonItem> listaCompleta;

    private RecyclerView recyclerView;
    private PokemonAdapterRV adapter;

    //elements
    private TextView loadingInfo;
    Button btnRetry;
    ProgressBar loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_list);

        listaCompleta = new ArrayList<>();

        service = PokeapiServiceProvider.getService();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new PokemonAdapterRV(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(this,3);
        recyclerView.setLayoutManager(layoutManager);

        //
        loadingInfo = (TextView) findViewById(R.id.loadingInfo);
        btnRetry = (Button) findViewById(R.id.btnRetry);
        loader = (ProgressBar) findViewById(R.id.loader);
        loadingInfo.setVisibility(View.GONE);
        btnRetry.setVisibility(View.GONE);

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerLista();
            }
        });

        obtenerLista();
    }

    public void obtenerLista()
    {
        btnRetry.setVisibility(View.GONE);
        loader.setVisibility(View.VISIBLE);
        loadingInfo.setText(R.string.loadingPokemonsInfo);
        loadingInfo.setVisibility(View.VISIBLE);

        Log.i(TAG,"Recuperando información...");
        service.ObtenerListaPokemon(1,0).enqueue(new Callback<PokemonList>() {
            @Override
            public void onResponse(Call<PokemonList> call, Response<PokemonList> response) {

                if(response.isSuccessful())
                {
                    btnRetry.setVisibility(View.GONE);
                    loader.setVisibility(View.VISIBLE);
                    loadingInfo.setText(R.string.loadingPokemonsData);
                    loadingInfo.setVisibility(View.VISIBLE);

                    Log.i(TAG," Información recuperada :)");
                    int count  = response.body().getCount();

                    Log.i(TAG,"Recuperando datos...");
                    service.ObtenerListaPokemon(count,0).enqueue(new Callback<PokemonList>() {
                        @Override
                        public void onResponse(Call<PokemonList> call, Response<PokemonList> response) {
                            if(response.isSuccessful())
                            {
                                loader.setVisibility(View.GONE);
                                loadingInfo.setVisibility(View.GONE);

                                listaCompleta = response.body().getResults();
                                Log.i(TAG," Datos recuperados :) " + listaCompleta.size());
                                adapter.adicionarListaPokemon(listaCompleta);
                            }
                            else
                            {
                                loadingInfo.setText(R.string.loadingFail);
                                btnRetry.setVisibility(View.VISIBLE);
                                loader.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onFailure(Call<PokemonList> call, Throwable t) {
                            Log.i(TAG,"Error al recuperar los datos!");

                            loadingInfo.setText(R.string.loadingFail);
                            btnRetry.setVisibility(View.VISIBLE);
                            loader.setVisibility(View.GONE);
                        }
                    });
                }
                else
                {
                    loadingInfo.setText(R.string.loadingFail);
                    btnRetry.setVisibility(View.VISIBLE);
                    loader.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<PokemonList> call, Throwable t) {
                Log.i(TAG,"Error al recuperar información!");

                loadingInfo.setText(R.string.loadingFail);
                btnRetry.setVisibility(View.VISIBLE);
                loader.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);

        //búsqueda
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                ArrayList<PokemonItem> filtro = new ArrayList<>();
                for (PokemonItem p: listaCompleta) {
                    if(p.getName().toLowerCase().contains(newText) || (p.getId()+"").contains(newText))
                    {
                        filtro.add(p);
                    }
                }
                adapter.establecerListaPokemon(filtro);
                return true;
            }
        });

        //acerca de
        MenuItem aboutItem = menu.findItem(R.id.action_acercaDe);
        aboutItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                try {
                    about();
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                return true;
            }
        });

        return true;
    }

    public void about() throws PackageManager.NameNotFoundException {
        String nombreVersion =
                getPackageManager().getPackageInfo
                (getPackageName(), 0).versionName;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.acerca_de_titulo) + " "+getString(R.string.app_name));
        builder.setMessage(Html.fromHtml(String.format(getString(R.string.acerca_de_mensaje), nombreVersion)));
        builder.setPositiveButton(R.string.acerca_de_btnAceptar, null);
        builder.create().show();
    }
}
