package simulacro.com.eduardorabanal.app.pokedex.adapters;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import simulacro.com.eduardorabanal.app.pokedex.R;
import simulacro.com.eduardorabanal.app.pokedex.activities.PokemonActivity;
import simulacro.com.eduardorabanal.app.pokedex.models.PokemonItem;

/**
 * Created by USER on 10/07/2017.
 */
public class PokemonAdapterRV extends RecyclerView.Adapter<PokemonAdapterRV.ViewHolder>{
    private ArrayList<PokemonItem> dataset;
    private Context context;

    public PokemonAdapterRV(Context context) {
        dataset = new ArrayList<>();
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pokemon_item_rv,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final PokemonItem p = dataset.get(position);
        holder.name.setText(p.getName() + "\n("+p.getId()+")");
        //holder.id.setText();

        Glide.with(context)
                .load(p.getImgUrl())
                .error(R.mipmap.ic_launcher)
                .into(holder.image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context,PokemonActivity.class);
                i.putExtra("id",p.getId());
                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(context, android.R.anim.fade_in, android.R.anim.fade_out);
                context.startActivity(i,options.toBundle());
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void adicionarListaPokemon(ArrayList<PokemonItem> pokemonItems) {
        dataset.addAll(pokemonItems);
        notifyDataSetChanged();
    }

    public void establecerListaPokemon(ArrayList<PokemonItem> pokemonItems) {
        dataset= pokemonItems;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        private ImageView image;
        private TextView name;
        //private TextView id;

        public ViewHolder(View itemView)
        {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.image);
            name = (TextView) itemView.findViewById(R.id.name);
            //id = (TextView) itemView.findViewById(R.id.id);
        }
    }
}
