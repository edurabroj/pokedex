package simulacro.com.eduardorabanal.app.pokedex.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import simulacro.com.eduardorabanal.app.pokedex.R;
import simulacro.com.eduardorabanal.app.pokedex.models.PokemonItem;
import simulacro.com.eduardorabanal.app.pokedex.pokeapi.PokeapiService;
import simulacro.com.eduardorabanal.app.pokedex.pokeapi.PokeapiServiceProvider;

public class PokemonActivity extends AppCompatActivity {
    private int pokemonId;
    private PokeapiService service;

    //elements
    ImageView image;
    TextView name, weight, height, types, loadingInfo, abilities;
    ProgressBar loader;
    LinearLayout layout;
    Button btnRetry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon);

        service= PokeapiServiceProvider.getService();
        pokemonId=getIntent().getExtras().getInt("id");

        //elements
        layout = (LinearLayout) findViewById(R.id.info);
        layout.setVisibility(View.GONE);
        image = (ImageView) findViewById(R.id.image);
        name = (TextView) findViewById(R.id.name);
        weight = (TextView) findViewById(R.id.weight);
        height = (TextView) findViewById(R.id.height);
        types = (TextView) findViewById(R.id.types);
        abilities = (TextView) findViewById(R.id.abilities);
        loader = (ProgressBar) findViewById(R.id.loader);
        loadingInfo = (TextView) findViewById(R.id.loadingInfo);
        btnRetry = (Button) findViewById(R.id.btnRetry);

        //visibility
        loader.setVisibility(View.VISIBLE);
        loadingInfo.setVisibility(View.VISIBLE);
        btnRetry.setVisibility(View.GONE);
        loadingInfo.setText(R.string.loadingPokemon);

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetDataSet();
            }
        });

        SetDataSet();
    }

    private void SetDataSet() {
        btnRetry.setVisibility(View.GONE);
        loader.setVisibility(View.VISIBLE);
        loadingInfo.setText(R.string.loadingPokemon);
        loadingInfo.setVisibility(View.VISIBLE);

        service.ObtenerPokemon(pokemonId).enqueue(new Callback<PokemonItem>() {
            @Override
            public void onResponse(Call<PokemonItem> call, Response<PokemonItem> response) {
                if(response.isSuccessful())
                {
                    loader.setVisibility(View.GONE);
                    loadingInfo.setVisibility(View.GONE);

                    layout.setVisibility(View.VISIBLE);
                    PokemonItem p = response.body();
                    Glide.with(getApplicationContext())
                            .load(p.getImgUrl())
                            .into(image);

                    name.setText(p.getName() + " (" + p.getId() + ")");
                    weight.setText(String.valueOf(p.getWeight()));
                    height.setText(String.valueOf(p.getHeight()));

                    String tipos = "";
                    for (PokemonItem.TypeObject t: p.getTypes()) {
                        tipos+=t.getType().getName() + "\n";
                    }
                    types.setText(tipos);

                    String habilidades = "";
                    for (PokemonItem.Abilitie t: p.getAbilities()) {
                        habilidades+=t.getAbility().getName() + "\n";
                    }
                    abilities.setText(habilidades);
                }
                else
                {
                    loadingInfo.setText(R.string.loadingBadResponse);
                    btnRetry.setVisibility(View.VISIBLE);
                    loader.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<PokemonItem> call, Throwable t) {
                loadingInfo.setText(R.string.loadingFail);
                btnRetry.setVisibility(View.VISIBLE);
                loader.setVisibility(View.GONE);
            }
        });
    }
}
