package simulacro.com.eduardorabanal.app.pokedex.pokeapi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 09/07/2017.
 */
public class PokeapiServiceProvider {
    public static PokeapiService getService()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(PokeapiService.class);
    }
}
