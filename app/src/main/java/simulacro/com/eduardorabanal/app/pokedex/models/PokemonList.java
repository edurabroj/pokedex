package simulacro.com.eduardorabanal.app.pokedex.models;

import java.util.ArrayList;

/**
 * Created by USER on 08/07/2017.
 */
public class PokemonList {
    private ArrayList<PokemonItem> results;
    private int count;

    public int getCount() {
        return count;
    }

    public ArrayList<PokemonItem> getResults() {
        return results;
    }
}
