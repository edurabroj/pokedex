package simulacro.com.eduardorabanal.app.pokedex.models;

/**
 * Created by USER on 08/07/2017.
 */
public class PokemonItem {
    private int id;
    private String name;
    private String url;

    private int weight;
    private int height;

    public class TypeObject
    {
        private TypeDetail type;

        public TypeDetail getType() {
            return type;
        }

        public void setType(TypeDetail type) {
            this.type = type;
        }
    }

    public class TypeDetail {
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    private Abilitie[] abilities;

    public Abilitie[] getAbilities() {
        return abilities;
    }

    public class Abilitie
    {
        private AbilitieDetail ability;

        public AbilitieDetail getAbility() {
            return ability;
        }

        public class AbilitieDetail
        {
            private String name;

            public String getName() {
                return name;
            }
        }
    }


    private TypeObject[] types;

    public TypeObject[] getTypes() {
        return types;
    }

    public void setTypes(TypeObject[] types) {
        this.types = types;
    }

    public int getWeight() {
        return weight;
    }

    public int getHeight() {
        return height;
    }

    public String getFullname()
    {
        return "#"+getId()+" "+ getName();
    }

    public String getImgUrl()
    {
        return "http://pokeapi.co/media/sprites/pokemon/"+ getId() +".png";
    }

    public int getId() {
        if(id>0)
        {
            return id;
        }
        else
        {
            String[] urlPartes = getUrl().split("/");
            return Integer.parseInt(urlPartes[urlPartes.length-1]);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
