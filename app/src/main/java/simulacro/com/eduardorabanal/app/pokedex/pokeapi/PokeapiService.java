package simulacro.com.eduardorabanal.app.pokedex.pokeapi;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import simulacro.com.eduardorabanal.app.pokedex.models.PokemonItem;
import simulacro.com.eduardorabanal.app.pokedex.models.PokemonList;

/**
 * Created by USER on 08/07/2017.
 */
public interface PokeapiService {
    @GET("pokemon")
    Call<PokemonList> ObtenerListaPokemon(@Query("limit") int limit, @Query("offset") int offset);

    @GET("pokemon/{id}")
    Call<PokemonItem> ObtenerPokemon(@Path("id") int id);
}
